/* eslint-disable no-console */
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Slider } from 'react-native';
// eslint-disable-next-line import/no-unresolved
import { RNCamera } from 'react-native-camera';

const landmarkSize = 40;

export default class CameraScreen extends React.Component {
  state = {
    flash: 'off',
    zoom: 0,
    autoFocus: 'on',
    depth: 0,
    type: 'front',
    whiteBalance: 'auto',
    ratio: '16:9',
    recordOptions: {
      mute: false,
      maxDuration: 5,
      quality: RNCamera.Constants.VideoQuality['288p'],
    },
    isRecording: false,
    canDetectFaces: true,
    faces: [],
    faceDetected: false,
    countDetected: 0,
    message: 'Dê um sorriso!',
    validations: {
      smile: false,
      notSmile:false,
      closeEyes: false,
      openEyes: false
    },
    detectedLiving:false,
    capturedPhoto:false
  };

  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }


  toggleFocus() {
    this.setState({
      autoFocus: this.state.autoFocus === 'on' ? 'off' : 'on',
    });
  }


  setFocusDepth(depth) {
    this.setState({
      depth,
    });
  }

  takePicture = async function () {
    if (this.camera) {
      const data = await this.camera.takePictureAsync();
      console.warn('takePicture ', data);
    }
  };

  takeVideo = async function () {
    if (this.camera) {
      try {
        const promise = this.camera.recordAsync(this.state.recordOptions);

        if (promise) {
          this.setState({ isRecording: true });
          const data = await promise;
          this.setState({ isRecording: false });
          console.warn('takeVideo', data);
        }
      } catch (e) {
        console.error(e);
      }
    }
  };

  toggle = value => () => this.setState(prevState => ({ [value]: !prevState[value] }));

  facesDetected = ({ faces }) => {
    if (faces.length === 1) {
      this.detecedFaceLiving()
      if(this.state.countDetected > 0){
        this.setNotSmile(faces)
        this.setSmile(faces)
        this.setOpenEyes(faces)
        this.setClosedEyes(faces)
      }
      this.setState({ faces, faceDetected: true, countDetected: this.state.countDetected + 1 })
      if(this.detecedFaceLiving()){
        this.setState({capturedPhoto:true})
      }
      this.faceNotDeteced()
    } else {
      this.setState({ faceDetected: false })
    }
  };

  setOpenEyes(faces){
    if (faces[0].leftEyeOpenProbability > 0.99 && faces[0].rightEyeOpenProbability > 0.99) {
      this.setState({ validations: { ...this.state.validations, openEyes: true } })
    }
  }

  setClosedEyes(faces){
    if (faces[0].leftEyeOpenProbability < 0.99 && faces[0].rightEyeOpenProbability < 0.99) {
      this.setState({ validations: { ...this.state.validations, closeEyes: true } })
    }
  }

  setSmile(faces){
    if (faces[0].smilingProbability > 0.98 && this.state.validations.notSmile) {
      this.setState({ faces, faceDetected: true, countDetected: this.state.countDetected + 1,  validations: { ...this.state.validations, smile: true } })
    }
  }

  setNotSmile(faces){
    if (faces[0].smilingProbability < 0.99) {
      this.setState({ validations: { ...this.state.validations, notSmile: true} })
    }
  }

  resetDetected = () => {
    this.setState({ faces: [], faceDetected: false, countDetected: 0, validations: { ...this.state.validations, smile: false, notSmile:false, closeEyes:false, openEyes:false  } })
  }

  detecedFaceLiving = () => {
    let response = true
    Object.entries(this.state.validations).forEach(([key, value]) => {
      if(!value){
        response = false
      }     
    });
    this.setState({detectedLiving:response})
    return response
  }

  // isNotSmile = () =>{
  //   return this.state.validations.notSmile
  // }

  // isSmile = () =>{
  //   return this.state.validations.smile
  // }

  // isCloseEyes = () => {
  //   return this.state.validations.closeEyes
  // }

  // isOpenEyes = () => {
  //   return this.state.validations.openEyes
  // }


  faceNotDeteced = () => {
    if(!this.state.capturedPhoto){
      if (this.state.countDetected === 1) {
        setTimeout(() => {
          this.resetDetected()
        }, 15000);
      }
    }
  }

  renderFace = ({ bounds, faceID, rollAngle, yawAngle }) => (
    <View
      key={faceID}
      transform={[
        { perspective: 600 },
        { rotateZ: `${rollAngle.toFixed(0)}deg` },
        { rotateY: `${yawAngle.toFixed(0)}deg` },
      ]}
      style={[
        styles.face,
        {
          ...bounds.size,
          left: bounds.origin.x,
          top: bounds.origin.y,
        },
      ]}
    >
      {/* <Text style={styles.faceText}>ID: {faceID}</Text> */}
      {/* <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text> */}
      {/* <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text> */}
      {/* <Text style={styles.faceText}>left ear: {this.state.faces[0].leftEarPosition}</Text> */}
    </View>
  );

  renderMessageCloseEyes() {
    return (
      <Text style={styles.faceText}>Pisque!</Text>
    )
  }

  renderMessageSmile() {

    console.log(this.state.validations, 'change sorriso')
    if (!this.state.detectedLiving) {
      return (
        <Text style={styles.faceText}>Sorria Feliz!</Text>
      )
    } else {
      return (
        <Text style={styles.faceText}>Obrigado!</Text>
      )
    }

  }

  renderLandmarksOfFace(face) {

    const renderLandmark = position =>
      position && (
        <View
          style={[
            styles.landmark,
            {
              left: position.x - landmarkSize / 2,
              top: position.y - landmarkSize / 2,
            },
          ]}
        />
      );
    return (
      <View key={`landmarks-${face.faceID}`}>
        {renderLandmark(face.leftEyePosition)}
        {renderLandmark(face.rightEyePosition)}
        {/* {renderLandmark(face.leftEarPosition)}
        {renderLandmark(face.rightEarPosition)}
        {renderLandmark(face.leftCheekPosition)}
        {renderLandmark(face.rightCheekPosition)}
        {renderLandmark(face.leftMouthPosition)}
        {renderLandmark(face.mouthPosition)}
        {renderLandmark(face.rightMouthPosition)}
        {renderLandmark(face.noseBasePosition)}
        {renderLandmark(face.bottomMouthPosition)} */}
      </View>
    );
  }

  renderFaces = () => (
    <View style={styles.facesContainer} pointerEvents="none">
      {this.state.faces.map(this.renderFace)}
    </View>
  );

  renderLandmarks = () => (
    <View style={styles.facesContainer} pointerEvents="none">
  {this.state.faces.map(this.renderLandmarksOfFace)}
    </View>
  );


  renderCamera() {
  // console.log(this.state.faces, "feces")
  

const { canDetectFaces, faceDetected } = this.state;
  return (
    <RNCamera
      ref={ref => {
        this.camera = ref;
      }} 
      style={{
          flex : 1,
    }}
    type={this.state.type}
        flashMode={this.state.flash}
    autoFocus = {this.state.autoFocus}
    zoom={this.state.zoom}
    whiteBalance={this.state.whiteBalance}
    ratio={this.state.ratio}
    focusDepth={this.state.depth}
    trackingEnabled  
    androidCameraPermissionOptions={{
        title: 'Permission to use ca m era',
      message: 'We need your permission to use your camera',
      buttonPositive: 'Ok',
      buttonNegative: 'Cancel',
    }}
    faceDetectionLandmarks={
  RNCamera.Constants.FaceDetection.Landmarks
        ? RNCamera.Constants.FaceDetection.Landmarks.all
        : undefined
        }
    faceDetectionClassifications={
  RNCamera.Constants.FaceDetection.Classifications
        ? RNCamera.Constants.FaceDetection.Classifications.all
        : undefined
        }
    onFacesDetected={canDetectFaces ? this.facesDetected : null}
      >   
        <View
      style={{
        flex: 0.5,
      }}
    >
        <View
  style={{
      backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-around',
      }} 
            >
            {/* <TouchableOpacity style={styles.flipButton} onPress={this.toggleFacing.bind(this)}>
    <Text style={styles.flipText}> FLIP </Text>
    
            </TouchableOpacity> */}
    </View>
    {this.renderMessageSmile()}
    {/* <Text style={styles.faceText}>Sorrisos:{this.state.validations.countSmiles}</Text> */}
          <View
      style={{
        backgroundColor: 'transparent',
        flexDirection: 'row',
        justifyContent: 'space-around',
      }}
    >
      {/* <TouchableOpacity onPress={this.toggle('canDetectFaces')} style={styles.flipButton}>
              <Text style={styles.flipText}>
                {!canDetectFaces ? 'Detect Faces' : 'Detecting Faces'}
              </Text>
            </TouchableOpacity> */}
    </View>  
        </View>   
    {faceDetected && this.renderFaces()}
      {faceDetected && this.renderLandmarks()}
      </ RNCamera>
        );
      }
    
  render() {
    return <View style={styles.container}>{this.renderCamera()}</View>;
    }
  }
  
const styles = StyleSheet.create({
        container: {
        flex: 1,
      paddingTop: 10,
      backgroundColor: '#000',
    },
  flipButton: {
        flex: 0.3,
      height: 40,
      marginHorizontal: 2,
      marginBottom: 10,
      marginTop: 10,
      borderRadius: 8,
      borderColor: 'white',
      borderWidth: 1,
      padding: 5,
      alignItems: 'center',
      justifyContent: 'center',
    },
  flipText: {
        color: 'white',
      fontSize: 15,
    },
  zoomText: {
        position: 'absolute',
      bottom: 70,
      zIndex: 2,
      left: 2,
    },
  picButton: {
        backgroundColor: 'darkseagreen',
      },
  facesContainer: {
        position: 'absolute',
      bottom: 0,
      right: 0,
      left: 0,
      top: 0,
    },
  face: {
        padding: 10,
      borderWidth: 2,
      borderRadius: 2,
      position: 'absolute',
      borderColor: '#FFD700',
      justifyContent: 'center',
      // backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
  landmark: {
        width: landmarkSize,
      height: landmarkSize,
      // padding:2,
      position: 'absolute',
      // backgroundColor: 'red',
      borderWidth: 2,
      borderRadius: 2,
      borderColor: '#FFD700',
      justifyContent: 'center',
    },
  faceText: {
        color: '#FFD700',
      fontWeight: 'bold',
      textAlign: 'center',
      margin: 10,
      fontSize:30,
      backgroundColor: 'transparent',
    },
  text: {
        padding: 10,
      borderWidth: 2,
      borderRadius: 2,
      position: 'absolute',
      borderColor: '#F00',
      justifyContent: 'center',
    },
  textBlock: {
        color: '#F00',
      position: 'absolute',
      textAlign: 'center',
      backgroundColor: 'transparent',
    },
});